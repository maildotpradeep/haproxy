FROM ubuntu:18.04

RUN apt-get update && apt-get install -y haproxy

ENTRYPOINT haproxy -d -f $cfg

EXPOSE 80
